use chrono::offset::TimeZone;
use chrono::{DateTime, Utc};
use rusqlite::{params, Connection, Result};

use std::env;

const TIME_FORMAT: &str = "%Y%m%dT%H%M%SZ";
const TIME_FORMAT_LOCAL: &str = "%Y%m%dT%H%M%S";

fn main() -> Result<()> {
    let db_path = env::args()
        .nth(1)
        .expect("first argument should be path to sqlite DB to modify");
    let conn = Connection::open(db_path)?;
    let mut ids = get_dirty_repeating_ids_terminating_in_future(&conn)?;

    println!("term-in-future ids: {:?}", ids);

    ids.extend(get_dirty_repeating_nonterminating_ids(&conn)?);
    println!("all ids: {:?}", ids);

    if ids.len() == 0 {
        println!("Nothing to fix here :)");
        return Ok(());
    }

    make_clean(&conn, &ids)?;
    println!("Fixed!");

    Ok(())
}

fn get_dirty_repeating_ids_terminating_in_future(conn: &Connection) -> Result<Vec<i64>> {
    let mut stmt = conn.prepare(r#"SELECT _id, rrule FROM Events WHERE _sync_id IS NULL AND INSTR(rrule, "UNTIL") > 0 AND rrule != "";"#)?;
    let untils_iter = stmt.query_map(params![], |row| -> Result<(i64, String)> {
        Ok((row.get(0)?, row.get(1)?))
    })?;

    let now = chrono::offset::Utc::now();
    let mut term_ids = vec![];
    for tup in untils_iter {
        let (id, rrule) = tup.expect("cannot deserialize query result tuple");
        let until = get_until(&rrule)
            .expect("corrupt Events table -- unexpected datatypes for _id, rrule columns");
        //println!("id: {}, until: {:?}, rrule: {:?}", id, until, rrule);
        if until > now {
            //println!("IN FUTURE");
            term_ids.push(id);
        }
    }
    Ok(term_ids)
}

fn get_dirty_repeating_nonterminating_ids(conn: &Connection) -> Result<Vec<i64>> {
    let mut stmt = conn.prepare(r#"SELECT _id FROM Events WHERE _sync_id IS NULL AND INSTR(rrule, "UNTIL") = 0 AND rrule != "";"#)?;
    let ids_iter = stmt.query_map(params![], |row| -> Result<i64> { Ok(row.get(0)?) })?;
    Ok(ids_iter.map(|opt_id| opt_id.unwrap()).collect())
}

fn make_clean(conn: &Connection, ids: &[i64]) -> Result<()> {
    let mut id_str = String::new();
    let mut first = true;
    for id in ids {
        if !first {
            id_str.push_str(", ");
        }
        first = false;
        id_str.push_str(&id.to_string());
    }
    let sql = format!(
        r#"UPDATE Events SET _sync_id = ('aaronfakesyncid' || _id), dirty = 0 WHERE _id IN ({});"#,
        id_str
    );
    let mut stmt = conn.prepare(&sql)?;
    let mut rows = stmt.query(params![])?;
    rows.next()?; // Without this, the query doesn't get executed
    Ok(())
}

fn get_until(rrule: &str) -> Option<DateTime<Utc>> {
    for term in rrule.split(";") {
        if term.starts_with("UNTIL=") {
            let datetime_str = term.strip_prefix("UNTIL=").unwrap();
            if datetime_str.ends_with("Z") {
                return Some(
                    chrono::offset::Utc
                        .datetime_from_str(datetime_str, TIME_FORMAT)
                        .expect(&format!("could not parse datetime str as utc: {:?}", datetime_str)),
                );
            } else {
                return Some(
                    chrono::offset::Local
                        .datetime_from_str(datetime_str, TIME_FORMAT_LOCAL)
                        .expect(&format!("could not parse datetime str as local: {:?}", datetime_str))
                        .into(), // Convert to UTC
                );
            }
        }
    }
    None
}
